# Kemeny Rank Aggregation

This project features a brute-force implementation of the _Kemeny Rank Aggregation_ problem as well as a heuristic approach for finding an approximate consensus ranking. The latter consists of transforming an instance into a TSP problem, subsequently solving it and using the tentative result as a starting solution for a genetic algorithm.

## Prerequisites

All scripts were tested with Python3.7. Please make sure that you have all required packages installed by executing

```
pip3 install -r requirements.txt
```

## Execution

To run the brute-force implementation that computes an optimal solution, simply run

```
python3 kemeny_rule.py <election-instance>
```

To run the genetic algorithm that computes an approximate consensus ranking, it suffices to run

```
python3 ga.py -i <election-instance>
```

To get more information about parameter settings for the genetic algorithm and such, execute

```
python3 ga.py -h
```

Unit tests can be executed with

```
python3 -m unittest tests.py 
```


## Folder structure

* data
    * bruteforce_dataset
    * f1_dataset
* experiments
    * ga
    * tu-berlin
* src

`data` contains randomly generated instances for testing the brute-force implementation (in `data/bruteforce_dataset`, generated using `experiments/generate_instances.py`), Formula 1 instances (in `data/f1_dataset`, obtained from [here](https://www.akt.tu-berlin.de/menue/software/)) to evaluate the performance of the genetic algorithm as well as 
the tiny election file `test.election` for testing purposes.

`experiments` comprises results of runs using an [implementation by the TU Berlin](https://www.akt.tu-berlin.de/menue/software/)
(in `experiments/tu-berlin`) and the genetic algorithm (in `experiments/ga`) as well as scripts for the conduction and evaluation of a comparison thereof.

`src` contains all the scripts for the brute-force implementation, the graph transformation and the heuristics applied to solve _Kemeny Rank Aggregation_ instances.

