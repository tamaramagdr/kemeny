import ga
from utils import *
import tsp, kemeny_rule

# path = "../data/sport_F1_1961.election"
path = "../data/bruteforce_dataset/inst_6_10_09-05-2020_15:17:36.election"

print("loading data...")
preferences, unique_header = parse_data(path)

print("constructing graph...")
dist, unique_header = construct_graph(path)

print("running kemeny_tsp...")
sol_array = tsp.kemeny(dist)
# print("array of solutions: ", sol_array)

num_generations = 100
hof_size = 3
max_time = 120
elitists = 0
selec_rate = 0.5  # between [0.5, 0.7, 0.9], consistently much better
tour_size = 6  # between 3, 6, 9 this is the one that looks most stable
repl_rate = 1.0  # between [0.5, 0.7, 0.9, 1.0], 0.9 and 1.0 are better
xprob = 0.3
mprob = 0.7
# probs between [0.3,0.7],[0.5,0.5],[0.7,0.3]

for _ in range(100):
    print(preferences, sol_array, unique_header, num_generations, selec_rate, tour_size, repl_rate, xprob, mprob,
          hof_size, elitists, max_time)
    hof = ga.ga(preferences, sol_array, unique_header, num_generations, selec_rate, tour_size, repl_rate, xprob, mprob,
                hof_size, elitists, max_time)
    print(hof[0], kemeny_rule.get_score(preferences, hof[0]))
    print("Best candidate: {0:s}".format(numeric_to_names(unique_header, hof[0])[0]))
    print()
    print("------")
