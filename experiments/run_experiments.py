import time
import os

import utils
import tsp
import ga


def run_optimal(path):

    f = open('./ga/' + os.path.split(path)[1] + '.solution', 'w')

    st = time.time()  # measure time for parsing
    print("loading data...")
    preferences, unique_header = utils.parse_data(path)

    print("constructing graph...")
    dist, unique_header = utils.construct_graph(path)

    f.writelines("Parsing\t{0:.4f}\n".format(time.time() - st))

    st = time.time()  # measure time for tsp
    print("running kemeny_tsp...")
    sol_array = tsp.kemeny(dist)
    print("array of solutions: ", sol_array)

    f.writelines("TSP\t{0:.4f}\n".format(time.time() - st))

    f.close()
    # get exact measurements of genetic algorithm
    hof = ga.ga(preferences, sol_array, unique_header, path='./ga/' + os.path.split(path)[1] + '.solution', num_generations=1)
    print(hof[0])
    print(utils.numeric_to_names(unique_header, hof[0]))


if __name__ == '__main__':
    for dir, subdir, files in os.walk('../data/f1_dataset/'):
        for file in files:
            if file.endswith('.election'):
                path = "../data/f1_dataset/" + file
                print(path)
                run_optimal(path)

