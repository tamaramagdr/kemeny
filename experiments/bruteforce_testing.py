import glob
from utils import *
import tsp
import kemeny_rule
import time
import numpy as np
import ga

# num_generations = 100
hof_size = 3
max_time = 120
elitists = 0
selec_rate = 0.5
tour_size = 6
repl_rate = 1.0
xprob = 0.3
mprob = 0.7

paths = glob.glob("../data/bruteforce_dataset/*.election")
paths = np.array(paths)
paths = np.sort(paths)

for path in paths:
    starting_time = time.time()
    inst_path = path[27:]
    inst_id = path[32:33]
    ncand = path[34:35]
    npref = path[36:37]
    if int(npref) == 1:
        npref = 10

    # ------------BRUTEFORCE------------
    preferences, unique_header = parse_data(path)
    dist, unique_header = construct_graph(path)
    brute_result = kemeny_rule.kemeny_bruteforce(preferences)
    brute_score = kemeny_rule.get_score(preferences, brute_result)

    bruteforce_time = time.time() - starting_time

    # ------------GA100--------------
    times100 = []
    scores100 = []
    for i in range(20):
        ga_starting_time = time.time()
        # doing it again to be fair
        # preferences, unique_header = parse_data(path)
        # dist, unique_header = construct_graph(path)
        sol_array = tsp.kemeny(dist)

        # print(preferences, sol_array, unique_header, num_generations, selec_rate, tour_size, repl_rate, xprob, mprob, hof_size, max_time)
        hof = ga.ga(preferences, sol_array, unique_header, 100, selec_rate, tour_size, repl_rate, xprob, mprob,
                    hof_size, elitists, max_time)

        ga_score100 = kemeny_rule.get_score(preferences, hof[0])
        ga_time100 = time.time() - ga_starting_time
        times100.append(ga_time100)
        scores100.append(ga_score100)

    times100 = np.array(times100)
    scores100 = np.array(scores100)
    ga_time100 = np.mean(times100)
    ga_score100 = np.mean(scores100)

    time_difference100 = ga_time100 - bruteforce_time
    diff_score100 = round((ga_score100 - brute_score) / brute_score, 2)

    # ------------GA1000--------------
    times1000 = []
    scores1000 = []
    for i in range(20):
        ga_starting_time = time.time()
        # doing it again to be fair
        # preferences, unique_header = parse_data(path)
        # dist, unique_header = construct_graph(path)
        sol_array = tsp.kemeny(dist)

        # print(preferences, sol_array, unique_header, num_generations, selec_rate, tour_size, repl_rate, xprob, mprob, hof_size, elitists, max_time)
        hof = ga.ga(preferences, sol_array, unique_header, 1000, selec_rate, tour_size, repl_rate, xprob, mprob,
                    hof_size, elitists, max_time)

        ga_score1000 = kemeny_rule.get_score(preferences, hof[0])
        ga_time1000 = time.time() - ga_starting_time
        times1000.append(ga_time1000)
        scores1000.append(ga_score1000)

    times1000 = np.array(times1000)
    scores1000 = np.array(scores1000)
    ga_time1000 = np.mean(times1000)
    ga_score1000 = np.mean(scores1000)

    time_difference1000 = ga_time1000 - bruteforce_time
    diff_score1000 = round((ga_score1000 - brute_score) / brute_score, 2)

    # ------------PRINT ------------------

    print(inst_path, " & ", ncand, " & ", npref, " & ", inst_id, " & ", brute_score, " & ", round(bruteforce_time, 4),
          "s & ", ga_score100, " & ", round(ga_time100, 4),
          "s & ", diff_score100, " & ", round(time_difference100, 4), "s & ", ga_score1000, " & ",
          round(ga_time1000, 4), "s & ", diff_score1000, " & ", round(time_difference1000, 4), "s \\\\ \hline")

print("all done")
