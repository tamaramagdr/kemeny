import unittest
import os
import itertools as it
import numpy as np
import multiprocessing as mp

import utils, ga, tsp, kemeny_rule


class TestUtils(unittest.TestCase):

    def setUp(self):
        self.path = '../data/test.election'
        self.header = ['apples', 'bananas', 'oranges', 'peaches']

    def test_parse_data(self):
        pref = [[0, 3, 1, 4], [3, 2, 1, 4], [0, 3, 4, 4]]
        res_pref, res_header = utils.parse_data(self.path)
        self.assertEqual(res_pref.tolist(), pref)
        self.assertEqual(res_header, self.header)

    def test_construct_graph(self):
        adj_matrix = [[0, 0, 0, 0, 0], [0, 0, 1, 1, 1], [0, 2, 0, 1, 3], [0, 2, 1, 0, 3], [0, 2, 0, 0, 0]]
        res_adj_matrix, res_header = utils.construct_graph(self.path)
        self.assertEqual(res_adj_matrix, adj_matrix)
        self.assertEqual(res_header, self.header)

    def test_numeric_to_names(self):
        unique_list = ['A', 'B', 'C', 'D', 'E', 'F']
        solution = [0, 3, 5, 4, 2, 1]
        transformed_solution = ['A', 'D', 'F', 'E', 'C', 'B']
        res = utils.numeric_to_names(unique_list, solution)
        self.assertEqual(res, transformed_solution)


class TestKemenyRule(unittest.TestCase):

    def test_kendall_tau(self):
        v = np.array([0, 3, 2, 1, 5, 8, 7, 6, 9, 4])
        w = np.array([0, 1, 2, 3, 4, 5, 6, 7, 8, 9])
        combinations = list(it.combinations(w, 2))
        kt_distance = 11
        res = kemeny_rule.kendall_tau(v, combinations)
        self.assertEqual(res, kt_distance)

    def test_score(self):
        pref = [[0, 2, 3, 1], [1, 0, 2, 3], [0, 3, 2, 1], [3, 0, 2, 1]]
        sol = [0, 1, 2, 3]
        score = 10
        res = kemeny_rule.get_score(pref, sol)
        self.assertEqual(res, score)

    def test_bruteforce(self):
        pref = [[0, 3, 1, 4], [3, 2, 1, 4], [0, 3, 4, 4]]
        kemeny_rankings = [[0, 3, 1, 2], [0, 3, 2, 1]]
        res = kemeny_rule.kemeny_bruteforce(pref)
        self.assertTrue(res == kemeny_rankings[0] or res == kemeny_rankings[1])


class TestTSP(unittest.TestCase):

    def setUp(self):
        self.path = '../data/test.election'

    def test_solution_valid(self):
        matrix, header = utils.construct_graph(self.path)
        sol = tsp.kemeny(matrix)
        valid_sol = [0, 1, 2, 3]
        self.assertEqual(set(sol), set(valid_sol))

    def test_solution_okay(self):
        """
        Test if solution is 'good enough', since heuristics might not return an optimal solution.
        This is done by computing the Kendall-Tau score between an optimal solution and the returned solution.
        """
        matrix, header = utils.construct_graph(self.path)
        sol = tsp.kemeny(matrix)
        optimal_sol = [0, 3, 1, 2]  # one of the two valid solutions is sufficient since KT-distances are equivalent
        combinations = list(it.combinations(optimal_sol, 2))
        kt_distance = kemeny_rule.kendall_tau(sol, combinations)
        self.assertTrue(kt_distance <= 0.75 * len(combinations))  # at least 25% of preferences are the same


class TestGA(unittest.TestCase):

    def setUp(self):
        self.pool = mp.Pool()

    def test_evaluate(self):
        pop = [[0, 1, 2, 3, 4], [4, 3, 2, 1, 0], [0, 2, 4, 1, 3], [1, 3, 0, 2, 4]]
        pref = [[3, 1, 4, 2, 0], [1, 3, 4, 2, 0]]
        fitness = [13, 7, 19, 7]
        res = self.pool.starmap(ga.evaluate, [[x, pref] for x in pop])
        self.assertEqual(res, fitness)

    def test_select(self):
        pop = [[0, 1, 2, 3, 4], [4, 3, 2, 1, 0], [0, 2, 4, 1, 3], [1, 3, 0, 2, 4]]
        pref = [[3, 1, 4, 2, 0], [1, 3, 4, 2, 0]]
        ratio = 0.5
        n_selected = round(ratio * len(pop))
        pop_fitness = self.pool.starmap(ga.evaluate, [[x, pref] for x in pop])

        scores = [kemeny_rule.get_score(pref, r) for r in pop]
        chosen_idx = [i for i in sorted(range(len(scores)), key=lambda k: scores[k])[:n_selected]]
        chosen = [pop[i] for i in chosen_idx]

        result = ga.tournament_selection(pop, pop_fitness, ratio, 3)

        self.assertEqual(len(result), n_selected)
        self.assertEqual(result, chosen)

    def test_randomize_2_pos(self):
        x, y = ga.randomize_2_pos(1000)
        self.assertNotEqual(x, y)
        self.assertLess(x, y)

    def test_generate_offspring(self):
        parent1 = [0, 1, 4, 3, 2]
        parent2 = [4, 3, 2, 1, 0]
        pos1 = 1
        pos2 = 3
        child = [3, 1, 4, 2, 0]
        result = ga.generate_offspring(parent1, parent2, pos1, pos2)
        self.assertEqual(result, child)

    def test_order_2p_crossover(self):
        parents = [[0, 1, 4, 3, 2], [4, 3, 2, 1, 0], [1, 3, 0, 2, 4]]
        result = ga.order_2p_crossover(parents, 1.0)
        self.assertEqual(len(result), len(parents) * (len(parents) - 1))
        self.assertEqual(len(set([len(child) for child in result])), 1)

    def test_mutate(self):
        child = [0, 1, 2, 3, 4]
        result = ga.mutate([child.copy()], 1.0)
        swapped = len(child) - sum([child[i] == result[0][i] for i in range(len(child))])
        self.assertEqual(swapped, 2)

    def test_replace(self):
        parents = [[0, 1, 2, 3, 4], [2, 3, 4, 1, 0]]
        children = [[4, 3, 2, 1, 0], [1, 3, 2, 0, 4]]
        pref = [[3, 1, 4, 2, 0], [1, 3, 4, 2, 0]]
        hof = [[0, 2, 4, 1, 3]]
        repl_ratio = 0.8

        pop_fitness = self.pool.starmap(ga.evaluate, [[x, pref] for x in parents])
        children_fitness = self.pool.starmap(ga.evaluate, [[x, pref] for x in children])
        hof_fitness = self.pool.starmap(ga.evaluate, [[x, pref] for x in hof])

        result_pop, result_hof, hof_fitness = ga.replace(parents, pop_fitness, children, children_fitness, repl_ratio, hof, hof_fitness, 0)

        self.assertEqual(len(result_pop), len(parents))
        self.assertEqual(sorted(result_pop), sorted(children))
        self.assertEqual(result_hof, [children[1]])

    def test_generate_items(self):
        sol = [0, 1, 2, 3, 4]
        neighbors = [sol, [1, 0, 2, 3, 4], [0, 2, 1, 3, 4], [0, 1, 3, 2, 4], [0, 1, 2, 4, 3]]

        result = ga.generate_items(sol)
        self.assertEqual(sorted(neighbors), sorted(result))

    def test_ga(self):
        sol_array = [3, 1, 2, 4, 0]
        pref = [[3, 1, 4, 2, 0], [1, 3, 4, 2, 0]]
        unique_header = ['A', 'B', 'C', 'D', 'E']
        result = ga.ga(pref, sol_array, unique_header, num_generations=50, hof_size=1, path='./unittest.solution')
        self.assertTrue(kemeny_rule.get_score(pref, result[0]) <= 0.25 * len(result[0]))

    def tearDown(self):
        if os.path.isfile('./unittest.solution'):
            os.remove('./unittest.solution')
