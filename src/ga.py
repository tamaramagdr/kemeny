from utils import *
import tsp, kemeny_rule

import math
import time
import random
import multiprocessing as mp
import argparse
import logging


def tournament_selection(pop, pop_fitness, selec_ratio, tour_size):
    chosen = set()
    unchosen = set(range(len(pop)))

    for _ in range(round(len(pop) * selec_ratio)):
        competitors = random.sample(unchosen, tour_size)
        best = competitors[0]
        for i in range(1, len(competitors)):
            curr = competitors[i]
            if pop_fitness[curr] < pop_fitness[best]:
                best = curr
        chosen.add(best)
        unchosen.remove(best)
    chosen_solutions = []
    for i in chosen:
        chosen_solutions.append(pop[i])
    return chosen_solutions


def randomize_2_pos(max):
    """
    Generates two positions in a 0-max range and makes sure they are different and that pos1 < pos2
    :param max: upper bound
    :return: generated positions
    """
    pos1 = random.randrange(0, max, 1)
    pos2 = random.randrange(0, max, 1)
    while pos2 == pos1:
        pos2 = random.randrange(0, max, 1)
    if pos1 > pos2:
        pos1, pos2 = pos2, pos1

    return pos1, pos2


def generate_offspring(parent1, parent2, pos1, pos2):
    """
    :param parent1: solution
    :param parent2: solution
    :param pos1: integer that represents position in parent1
    :param pos2: integer that represents position in parent2

    parent1 gives its elements from pos1 (included) to pos2 (excluded) to the offspring,
    the remaining elements are inserted in the order they appear in parent2

    :return: the child generated
    """

    center = parent1[pos1:pos2]
    newstuff = []
    offspring = []

    for i in range(len(parent2)):
        if parent2[i] not in center:
            newstuff.append(parent2[i])

    for el in newstuff[:pos1]:
        offspring.append(el)
    for el in parent1[pos1:pos2]:
        offspring.append(el)
    for el in newstuff[pos1:]:
        offspring.append(el)

    return offspring


def order_2p_crossover(individuals, crossover_prob):
    # gets individuals and generates children with an order 2points crossover.
    # randomize 2 points: between those 2 points takes parent1.
    # Completes the child using the order of parent2.

    children = []
    # go through all the parent1-parent2 combinations and
    # cross them over with a certain probability

    for pp1 in range(len(individuals)):
        for pp2 in range(pp1 + 1, len(individuals)):
            parent1 = individuals[pp1]
            parent2 = individuals[pp2]

            if random.random() < crossover_prob:
                # with probability
                # randomize the two points
                pos1, pos2 = randomize_2_pos(len(parent1))

                # create offspring1 parent1->parent2
                offspring1 = generate_offspring(parent1, parent2, pos1, pos2)
                children.append(offspring1)

                # create offspring2 parent2->parent1
                offspring2 = generate_offspring(parent2, parent1, pos1, pos2)
                children.append(offspring2)

    return children


def mutate(children, mutation_prob):
    # swap two elements in a child.
    for child in children:
        if random.random() < mutation_prob:
            i, j = randomize_2_pos(len(child))
            child[i], child[j] = child[j], child[i]

    return children


def replace(parents, pop_fitness, children, children_fitness, repl_ratio, hof, hof_fitness, n_elitists):
    # replacement from HOT13
    pop_size = len(parents) - n_elitists
    hof_size = len(hof)

    children_needed = round(repl_ratio * pop_size)
    num_children = min(children_needed, len(children))
    num_parents = pop_size - num_children

    # get best elitist parents to stay
    elite = []
    for _ in range(n_elitists):
        max_fit = max(pop_fitness)
        p = parents[pop_fitness.index(max_fit)]
        elite.append(p)
        parents.remove(p)
        pop_fitness.remove(max_fit)

    # chosen_children = random.sample(children, num_children)
    if num_children:
        chosen_children, chosen_children_fitness = zip(*random.sample(list(zip(children, children_fitness)), num_children))
        chosen_children = list(chosen_children)
        chosen_children_fitness = list(chosen_children_fitness)
    else:
        chosen_children = []
        chosen_children_fitness = []
    chosen_parents = random.sample(parents, num_parents)

    for idx, individual in enumerate(chosen_children):

        # update hof
        for i in range(hof_size):
            # check if we already have the solution
            if individual == hof[i]:
                break
            # if the new solution is better, add it to the hof in order, and drop the last element from the hof
            if chosen_children_fitness[idx] < hof_fitness[i]:
                hof = hof[:i] + [individual] + hof[i:-1]
                hof_fitness[i] = chosen_children_fitness[idx]
                break

    new_gen = chosen_parents + chosen_children + elite

    return new_gen, hof, hof_fitness


def ga(preferences, sol_array, unique_header, num_generations=200, selec_ratio=0.5, tour_size=3, repl_ratio=0.9,
       crossover_prob=0.7, mutation_prob=0.15, hof_size=3, elitists=2, max_time=math.inf, path='test.solution'):
    """
    :param preferences: the preferences used to evaluate each generation
    :param sol_array: the tentative TSP solution
    :param num_generations: the number of generations
    :param pop_size: the max number of individuals that are in a population
    :param selec_ratio: the selection ratio
    :param tour_size: size of the tournament for the selection process
    :param repl_ratio: the replication ratio
    :param crossover_prob: probability to crossover
    :param mutation_prob: probability of a mutation to occur
    :param hof_size: the number of rankings in the hall of fame
    :param max_time: the maximum number of time that the program may run
    :param path: file name where solution will be stored

    :return: the hall of fame containing the best rankings
    """

    st = time.time()
    pop = generate_items(sol_array)

    # sneakily append some random individuals to the population to ensure diversity
    for _ in range(len(pop)):
        pop.append(random.sample(range(len(pop[0])), len(pop[0])))

    random.shuffle(pop)

    logging.debug("starting pop: {0}".format(pop))

    starting_time = time.process_time()

    pool = mp.Pool()

    # compute fitness of each individual
    pop_fitness = pool.starmap(evaluate, [[x, preferences] for x in pop])

    # sort individuals according to their fitness value
    hof = [x for _, x in sorted(zip(pop_fitness, pop), key=lambda pair: pair[0], reverse=False)][:hof_size]

    hof_fitness = pool.starmap(evaluate, [[x, preferences] for x in hof])
    logging.debug("Population generation + initial fitness evaluation took {0:.4f}s".format(time.time() - st))
    # open file here and write in file
    f = open(path, 'a')
    f.writelines("Init\t{0:.4f}\n".format(time.time() - st))
    f.close()

    for gen in range(num_generations):
        st = time.time()

        chosen_ones = tournament_selection(pop, pop_fitness, selec_ratio, tour_size)
        children = order_2p_crossover(chosen_ones, crossover_prob)
        children = mutate(children, mutation_prob)

        children_fitness = pool.starmap(evaluate, [[x, preferences] for x in pop])

        pop, hof, hof_fitness = replace(pop, pop_fitness, children, children_fitness, repl_ratio, hof, hof_fitness,
                                        elitists)

        pop_fitness = pool.starmap(evaluate, [[x, preferences] for x in pop])

        logging.debug("Generation {0:d} took {1:.4f}s".format(gen, time.time() - st))
        logging.debug("Fitness of HOF: {0}".format(hof_fitness))
        logging.debug("Best candidate: {0:s}".format(numeric_to_names(unique_header, hof[0])[0]))
        f = open(path, 'a')
        f.writelines(
            "Gen {0:d}\t{1:.4f}\t{2:s}\n".format(gen, time.time() - st, numeric_to_names(unique_header, hof[0])[0]))
        f.flush()
        f.close()

        if time.process_time() - starting_time > max_time:
            logging.info("timeout reached")
            return hof

    # return the hall of fame
    pool.close()
    return hof


def evaluate(individual, preferences):
    return kemeny_rule.get_score(preferences, individual)


def generate_items(sol):
    """
    Calculates all the neighbours from a single solution via adjacent swaps and creates a list with all of them
    :param sol: a tentative "good" solution
    :return: the solution and all its neighbours
    """
    items = [sol]

    for i in range(len(sol) - 1):
        new_item = sol.copy()
        new_item[i], new_item[i + 1] = new_item[i + 1], new_item[i]
        items.append(new_item)

    return items


if __name__ == "__main__":

    logger = logging.getLogger()

    parser = argparse.ArgumentParser(description='Run genetic algorithm for a Kemeny Rank Aggregation instance')
    parser.add_argument('--input', '-i', type=str, help='path to election instance',
                        default="../data/f1_dataset/sport_F1_1961.election")
    parser.add_argument('--output', '-o', type=str, help='path to solution file', default="./test.solution")
    parser.add_argument('--generations', '-g', type=int, help='number of generations to run genetic algorithm',
                        default=100)
    parser.add_argument('--selection', '-s', type=float, help='selection ratio for genetic algorithm', default=0.5)
    parser.add_argument('--replacement', '-r', type=float, help='replacement ratio for genetic algorithm', default=1.0)
    parser.add_argument('--tour_size', '-t', type=int, help='tour size for tournament selection', default=6)
    parser.add_argument('--crossover', '-c', type=float, help='crossover probability for crossover operation',
                        default=0.3)
    parser.add_argument('--mutation', '-m', type=float, help='mutation probability for an individual', default=0.7)
    parser.add_argument('--hof', '-f', type=int, help='size of the hall of fame (=best individuals)', default=3)
    parser.add_argument('--elitism', '-e', type=int,
                        help='number of best individuals that are kept in each generation (elitists)', default=0)
    parser.add_argument('--max_time', '-b', type=float,
                        help='maximum amount of time - when time limit reached, the algorithm stops (if not specified, no upper limit)',
                        default=math.inf)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--quiet', '-q', action='store_true',
                       help='silences all logging statements - mutually exclusive with -d')
    group.add_argument('--debug', '-d', action='store_true',
                       help='sets log level to debug (logs everything instead of just info statements) - mutually exclusive with -q')

    args = vars(parser.parse_args())

    path = args['input']
    n_gen = args['generations']
    selec_ratio = args['selection']
    repl_ratio = args['replacement']
    tour_size = args['tour_size']
    crossover_prob = args['crossover']
    mutation_prob = args['mutation']
    hof_size = args['hof']
    elitists = args['elitism']
    max_time = args['max_time']
    output_file = args['output']
    quiet = args['quiet']
    debug = args['debug']

    if quiet:
        logger.setLevel(logging.WARNING)
    elif debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)

    logging.info("loading data...")
    preferences, unique_header = parse_data(path)

    logging.info("constructing graph...")
    dist, unique_header = construct_graph(path)

    logging.info("running kemeny_tsp...")
    sol_array = tsp.kemeny(dist)
    logging.debug("array of solutions: {0}".format(sol_array))

    if tour_size > len(sol_array):
        tour_size = len(sol_array)
        logging.debug("changed tour size to {0}".format(tour_size))

    hof = ga(preferences, sol_array, unique_header, num_generations=n_gen, selec_ratio=selec_ratio, tour_size=tour_size,
             repl_ratio=repl_ratio, crossover_prob=crossover_prob, mutation_prob=mutation_prob, hof_size=hof_size,
             elitists=elitists, max_time=max_time, path=output_file)

    logging.debug(hof[0])
    logging.debug(numeric_to_names(unique_header, hof[0]))
