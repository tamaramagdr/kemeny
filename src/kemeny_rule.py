import itertools as it
import numpy as np
import logging
import sys

from utils import parse_data, construct_graph


def get_score(preferences, ranking):
    """
    Computes the Kemeny-score by taking the sum of all KT-distances between the solution ranking and the preferences
    :param preferences: an M x N matrix containing the preferences encoded as integers where M is the number of people
    expressing a preference for N candidates
    :param ranking: a possible consensus ranking
    :return: the Kemeny-score between the solution ranking and the preferences
    """
    combs = list(it.combinations(ranking, 2))  # get all pairs of candidates
    score = sum([kendall_tau(np.array(pref), combs) for pref in preferences])
    return score


def kendall_tau(pref, combinations):
    """
    Computes the number of discordant pairs between a preference and a ranking (already broken into ranked pairs)
    :param pref: a list containing a ranked preference
    :param combinations: ranked pairs ([a,b] stands for a>b) representing a solution ranking
    :return: the number of discordant pairs between the ranking and the preference
    """
    sum = 0
    for c in combinations:
        idx_left = np.where(np.array(pref) == c[0])[0]
        idx_right = np.where(np.array(pref) == c[1])[0]
        if idx_left.size == 0 and idx_right.size > 0:
            sum += 1
        elif idx_right.size > 0 and idx_left[0] > idx_right[0]:
            sum += 1
    return sum


def kemeny_bruteforce(preferences):
    """
    Creates a trivial solution (0,1,...,n-1) and calculates the Kemeny-score for each permutation of that solution
    :param preferences is an M x N matrix with M people expressing a ranking for N candidates
    :return: the solution with the highest Kemeny-score
    """

    curr_best = list(range(len(preferences[0])))
    curr_best_score = get_score(preferences, curr_best)
    perm = it.permutations(curr_best)

    for idx, perm in enumerate(perm):
        logging.debug("At permutation {0}".format(idx))
        s = get_score(preferences, perm)
        if s < curr_best_score:
            curr_best = perm
            curr_best_score = s
    return list(curr_best)


if __name__ == "__main__":

    if len(sys.argv) == 1:
        print("Please specify a path to an election instance")
        sys.exit(1)

    path = sys.argv[1]
    print("loading data...")
    preferences, unique_header = parse_data(path)
    print("constructing graph...")
    dist, unique_header = construct_graph(path)

    print("running bruteforce algorithm... ")
    result = kemeny_bruteforce(preferences)
    print("result: {0} with score of {1:d}".format(result, get_score(preferences, result)))
