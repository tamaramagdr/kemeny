import numpy as np
import networkx as nx
import itertools as it
import logging

logging.basicConfig(level=logging.INFO)


def parse_data(path):
    """
    :param path: path to the input file containing the preferences
    :return: an M x N matrix containing the preferences encoded as integers where M is the number of people
    expressing a preference for N candidates and a list containing the names of the candidates
    """
    with open(path) as f:
        data = [x.strip() for x in f.readlines()]

    # extract header
    header = []
    for line in data:
        header.extend(line.split(">"))
    unique_header = sorted(list(set(header)))

    preferences = np.full([len(data), len(unique_header)], len(unique_header))

    # extract preferences
    for vote_idx, vote in enumerate(data):
        for elem_idx, elem in enumerate(vote.strip().split(">")):
            preferences[vote_idx][elem_idx] = unique_header.index(elem)

    return preferences, unique_header


def construct_graph(path):
    """
    :param path: path to the input file containing the preferences
    :return: a list of lists that corresponds to the adjacency matrix for the directed complete graph with edges
    (in both directions for every pair of nodes) marked with weights that represent the number of disagreements
    for the preference relation (edge i->j denotes i<j and weight is number of times that i>j)
    """
    preferences, unique_header = parse_data(path)
    g = nx.MultiDiGraph()

    logging.debug(preferences)

    edges = [(-1, x, 0) for x in range(len(unique_header))]

    nodes = list(range(-1, len(unique_header)))

    logging.debug("nodes " + str(nodes))

    g.add_nodes_from(nodes)

    combinations = list(it.combinations(nodes[1:], 2))
    combinations.extend([x[::-1] for x in combinations])
    combs_dict = {key: 0 for key in combinations}

    for c in combs_dict.keys():
        sum = 0
        for pref in preferences:
            idx_left = np.where(pref == c[0])[0]
            idx_right = np.where(pref == c[1])[0]
            if idx_left.size == 0 and idx_right.size > 0:
                sum += 1
            elif idx_right.size > 0 and idx_left > idx_right:
                sum += 1
        combs_dict[c] = sum

    edges += [key + (combs_dict[key],) for key in combs_dict]
    logging.debug("edges " + str(edges))

    g.add_weighted_edges_from(edges)

    adj_matrix = nx.adjacency_matrix(g, weight='weight')

    return adj_matrix.todense().tolist(), unique_header


def numeric_to_names(unique_header, solution_arr):
    """
    :param unique_header: a list containing the names of the candidates
    :param solution_arr: a solution ranking
    :return: the solution ranking with the names of the candidates (instead of the integer encoding)
    """
    return [unique_header[x] for x in solution_arr]
